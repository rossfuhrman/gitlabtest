﻿using Npgsql;
using System;

namespace Dependencies
{
    public class SqlDatabaseConnection
    {
        public NpgsqlConnection conn;
        public string password;

        public SqlDatabaseConnection()
        {
            NpgsqlConnectionStringBuilder builder = new NpgsqlConnectionStringBuilder();
            builder.Password = "SecretP@ssword!";
            password = builder.Password;

            conn = new NpgsqlConnection(builder.ToString());
        }
    }
}
