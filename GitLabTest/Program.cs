﻿using Dependencies;
using System;

namespace GitLabTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlDatabaseConnection conn = new SqlDatabaseConnection();
            Console.WriteLine($"My password is {conn.password}");
        }
        
        [RedirectingAction]
        public ActionResult Download(string fileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(Server.MapPath("~/ClientDocument/") + fileName);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}
